# Naive Bayes Algorithm

lists<- read.csv('test.csv', stringsAsFactors = FALSE)
str(list)
lists$category <- as.factor(lists$category)
Encoding(lists$text) <- "UTF-8"

#text mining 
install.packages("tm")
library(tm)
install.packages("wordcloud")
library(wordcloud)
install.packages("e1071")
library(e1071)

#Generating DTM
spam_corpus <- Corpus(VectorSource (lists$text))

spam_corpus[[1]][[1]]

spam_corpus[[2]][[1]]


Clean_corpus <- tm_map(spam_corpus, removePunctuation)

Clean_corpus[[1]][[1]]

#clean white space
Clean_corpus <- tm_map(Clean_corpus, stripWhitespace)

#Clean_corpus <- tm_map(Clean_corpus, content_transformer(tolower))

hebrew_stopwords <- read.csv("stopwords.csv")$word

stopwords()

Clean_corpus <- tm_map(Clean_corpus, removeWords, c(stopwords(),hebrew_stopwords))


dtm <- DocumentTermMatrix(Clean_corpus)

dim(dtm)

#remove infrequent word  
frequent_dtm <-  DocumentTermMatrix(Clean_corpus, list(dictionary  = findFreqTerms(dtm,10)))
dim(frequent_dtm)  

#Data Visualization
pal <- brewer.pal(9, 'Dark2')

wordcloud(Clean_corpus, min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(Clean_corpus[lists$category == 'support'], min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(Clean_corpus[lists$category == 'sales'], min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(Clean_corpus[lists$category == 'block'], min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(Clean_corpus[lists$category == 'coop'], min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(Clean_corpus[lists$category == 'stop'], min.freq = 5, random.order = FALSE, colors = pal)

#use only data with catrgory to build the model
df_with_cat<-lists[lists$category != '',]
Clean_corpus_with_cat<-Clean_corpus[lists$category != '']
frequent_dtm_with_cat <-  DocumentTermMatrix(Clean_corpus_with_cat, list(dictionary  = findFreqTerms(dtm,10)))

#spliting into training set and test set

split <- runif(500)

split <- split >0.3

#dividing raw data
train_raw <- df_with_cat[split,]
test_raw <- df_with_cat[!split,]

#dividing clean corpus
train_courpus <- Clean_corpus_with_cat[split]
test_courpus <- Clean_corpus_with_cat[!split]

#dividing dtm
train_dtm <- frequent_dtm_with_cat[split,]
test_dtm <- frequent_dtm_with_cat[!split,]

#convert the DTM into yes/no

#conv_yesno <- function(x){
#  x <- ifelse(x>0,1,0)
#  x <- factor(x, levels = c(1,0), labels = c('Yes','No'))
#}

#Margin 1:2  
#train <- apply(train_dtm,  MARGIN = 1:2, conv_yesno)
#test <- apply(test_dtm,  MARGIN = 1:2, conv_yesno)

#df_train <- as.data.frame(train_dtm)
#df_test <- as.data.frame(test_dtm)


#Add the type column
train_dtm$type <- train_raw$type
test_dtm$type <- test_raw$type

train_dtm[,25]
dim(train_dtm)

#Generating the model using naive bayes
model <- naiveBayes(train_dtm[,-25],train_dtm$type)


prediction <- predict(model,test_dtm[,-25] )
